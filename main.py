import statusio
import requests
import datetime

configuration = {}
module_name='statusio'
module_description='Submodule for the status.io public API'
commands = ['status', 'incidents']
config_section = module_name.lower()
command_help={
    'status': 'fetch status information',
    'incidents': 'fetch incident list or create new incident'
    }
def execute(command, params, app_config):
    global configuration
    configuration = app_config
    if command == 'status':
        statusio_status(params)
    elif command == 'incidents':
        statusio_incidents(params)

def statusio_status(params):
    global configuration
    if len(params) != 0:
        api = statusio.Api(api_id=configuration[config_section]['id'], api_key=configuration[config_section]['key'])
        summary = (api.StatusSummary(configuration[config_section]['page_id']))
        results = summary.get('result')
    else:
        print('Possible params:', ['all', 'services'])
    if 'all' in params:
        print('Querying overall status, times are in UTC')
        status_overall = results.get('status_overall')
        last_update = datetime.datetime.strptime(status_overall.get('updated')[:-5]+'Z', '%Y-%m-%dT%H:%M:%SZ') 
        print_status(time=last_update, status=status_overall.get('status'), name='Overall')
    elif 'services' in params:
        print('Querying service status, times are in UTC')
        for service in results.get('status'):
            last_update = datetime.datetime.strptime(service.get('updated')[:-5]+'Z', '%Y-%m-%dT%H:%M:%SZ') 
            print_status(time=last_update, name=service.get('name'), status=service.get('status'))
def statusio_incidents(params):
    OK = '\033[92m' # green
    FAIL = '\033[91m' # red
    MONITORING = '\033[94m' # blue
    ENDC = '\033[0m' # reset styling
    global configuration
    if len(params) == 0:
        print('Possible params:', ['list', 'details'])
    else:
        api = statusio.Api(api_id=configuration[config_section]['id'], api_key=configuration[config_section]['key'])
    if 'list' in params:
        incidents = api.IncidentListByID(configuration[config_section]['page_id'])
        results = incidents.get('result')
        if len(params) > 1:
            active = True if params[1] == 'all' or params[1] == 'active' else False
            resolved = True if params[1] == 'all' or params[1] == 'resolved' else False
        else:
            active, resolved = True, True
        if active and len(results.get('active_incidents')) > 0:
            print('Fetching active incident list, times are in UTC')
            for incident_id in results.get('active_incidents'):
                incident = api.IncidentSingle(configuration[config_section]['page_id'], incident_id)
                datetime_open = datetime.datetime.strptime(incident.get('datetime_open')[:-5]+'Z', '%Y-%m-%dT%H:%M:%SZ') 
                print(f'{str(datetime_open):<20}{FAIL} ID: {incident_id} - {incident.get("name")}{ENDC}')
        elif active and len(results.get('active_incidents')) == 0:
            print('No active incidents')
        if resolved and len(results.get('resolved_incidents')) > 0:
            print('Fetching resolved incident list, times are in UTC')
            for incident_id in results.get('resolved_incidents'):
                incident = api.IncidentSingle(configuration[config_section]['page_id'], incident_id).get('result')[0]
                datetime_open = datetime.datetime.strptime(incident.get('datetime_open')[:-5]+'Z', '%Y-%m-%dT%H:%M:%SZ') 
                print(f'{str(datetime_open):<20}{OK} ID: {incident_id} - {incident.get("name")}{ENDC}')
    if 'details' in params:
        if len(params) > 1:
            incident_id = ''.join(params[1:])
            incident = api.IncidentSingle(configuration[config_section]['page_id'], incident_id).get('result')[0]
            datetime_open = datetime.datetime.strptime(incident.get('datetime_open')[:-5]+'Z', '%Y-%m-%dT%H:%M:%SZ') 
            print(f'{str(datetime_open):<20}{OK}ID: {incident_id} - {incident.get("name")}{ENDC}')
            print('-'*25)
            for message in incident.get('messages'):
                message_date = datetime.datetime.strptime(message.get('datetime')[:-5]+'Z', '%Y-%m-%dT%H:%M:%SZ') 
                if message.get('state') == 400:
                    message_state = f'{OK}resolved{ENDC}'
                elif message.get('state') == 300:
                    message_state = f'{MONITORING}monitoring{ENDC}'
                else:
                    message_state = f'{FAIL}{message.get("state")}{ENDC}'
                print(f'Author: {message.get("user_full_name")} <{message.get("user_email")}>')
                print(f'State: {message_state}')
                print(f'Date: {message_date}')
                print(f'Message:\n{message.get("details")}')
                print('-'*25)


def print_status(name=None, status=None, time=None):
    # just for output styling
    OK = '\033[92m' # green
    MAINTENANCE = '\033[94m' # blue
    WARNING = '\033[93m' # yellow
    FAIL = '\033[91m' # red
    ENDC = '\033[0m' # reset styling
    if status and name and time:
        output = str(time)
        if status.lower() == 'operational':
            output += OK
        elif status.lower() == 'planned maintenance':
            output += MAINTENANCE
        elif status.lower() == 'security issue':
            output += FAIL
        elif status.lower() == 'service disruption':
            output += FAIL
        elif status.lower() == 'partial service disruption':
            output += WARNING
        elif status.lower() == 'degraded performance':
            output += WARNING
        else:
            output += WARNING
        output += f' {name:<60}-{status:>30}{ENDC}'
        print(output)