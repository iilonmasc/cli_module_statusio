# statusio cli submodule

This submodule needs the statusio api library https://github.com/statusio/statusio-python

Installation of the package can be done with
```
$ pip install statusio-python
```


## Installation

Your distribution of the custom cli should come with the `module` command, simply clone this repo, change directory into it (`cd cli_module_statusio`) and run `ccli module link` to link the module to your custom cli

```
# clone this repository
$ git clone https://gitlab.com/iilonmasc/cli_module_statusio.git

# change directory into the repository
$ cd cli_module_statusio

# link the module to your ccli installation
$ ccli module link
```

## Configuration

Add a `statusio` section to your configuration file

```
[statusio]
id=<your-user-id
key=<your-api-key>
page_id=<your-page-id>
```
